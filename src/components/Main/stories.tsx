import { Meta, Story } from '@storybook/react'
import Main from '.'

export default {
    title: 'Main',
    component: Main,

    // Argumetos default do Componente
    args: {
        title: 'React Avançado!',
        description: 'TypeScript, ReactJS, NextJS e Styled Components'
    }
} as Meta

export const Basic: Story = (args) => <Main {...args} />

// Sobrescreve os argumentos para um item específico
Basic.args = {
    title: 'React Avançado!',
    description: 'TypeScript, ReactJS, NextJS e Styled Components'
}
