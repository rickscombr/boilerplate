import { AppProps } from 'next/app'
import Head from 'next/head'

import GlobalStyles from 'styles/globals'

function App({ Component, pageProps }: AppProps) {
    return (
        <>
            <Head>
                <title>React Avançado - Boilerplate</title>
                <link rel="shortcut icon" href="/imgs/icon-512.png" />
                <link rel="apple-touch-icon" href="/imgs/icon-512.png" />
                <link rel="manifest" href="/manifest.json" />
                <meta name="theme-color" content="#317EFB" />
                <meta
                    name="description"
                    content="A simple project starter to work with TypeScript, React, NextJS and Styled Components"
                />
            </Head>
            <GlobalStyles />
            <Component {...pageProps} />
        </>
    )
}

export default App
