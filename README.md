# Boilerplate - NextJs

Este projeto usa:

* NextJs
* React
* TypeScript
* Babel
* Styled Components
* Storybook
* Jest
* Testing Library
* Prettier
* ESLint
* List Staged
* Husky

## Pré-requisitos

* [Git](https://git-scm.com/)
* [Node](http://nodejs.org/)

## Respositório na núvem

* [GitHub](https://github.com/rickscombr/boilerplate.git)

## Criando novo projeto

Para criar novo projeto com base neste boilerplate basta executar o comando abaixo:

```sh
$ yarn create next-app -e https://github.com/rickscombr/boilerplate
```
Será solicitado informar o <NOME_PROJETO> e pressionar ENTER.

## Instalação

Para realizar a instalação basta acessar a pasta e executar o comando yarn

```sh
$ cd <NOME_PROJETO>
$ yarn
```

## Executando Local (DEV)

```sh
$ NODE_ENV=development
$ yarn dev
```
O projeto será executado em http://localhost:3000
## Executando Storybook (local)

```sh
$ yarn storybook
```
O projeto será executado em http://localhost:6006

## Deploy para produção

```sh
$ yarn build
```

## Executando produção (PROD)

```sh
$ NODE_ENV=production
$ yarn start
```
